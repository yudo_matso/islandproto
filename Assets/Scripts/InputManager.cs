﻿using System;
using Holoville.HOTween;
using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{

    public PlayerController Player;

    private int _dragLayerMask;
    private bool _isDrag = false;
    private bool _isDragCharacter = false;


    private float _lastTap;
    public float TapTime;

    void Start()
    {
        _dragLayerMask = 1 << LayerMask.NameToLayer("Drag");
    }

    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~_dragLayerMask))
            {
                if (Input.GetTouch(0).tapCount == 2 && Input.GetTouch(0).deltaTime < TapTime)
                {
                    Player.Fire(hit.point);
                    _isDrag = false;
                }
                else
                {
                    _lastTap = Time.time;
                    _isDrag = true;
                }
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~_dragLayerMask))
            {
                if ((Time.time - _lastTap) < TapTime)
                {
                    Player.Fire(hit.point);
                    _isDrag = false;
                }
                else
                {
                    _lastTap = Time.time;
                    _isDrag = true;
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _isDrag = false;

        }

        if (_isDrag)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                Player.Move(hit.point);
            }
        }
    }
}
