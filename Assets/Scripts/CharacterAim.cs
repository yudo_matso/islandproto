﻿using Holoville.HOTween;
using UnityEngine;
using System.Collections;

public class CharacterAim : MonoBehaviour
{
    public GameObject Visor;
    public Vector3 targetPosition;
    public bool IsValid { get; private set; }

    void OnEnable()
    {
        IsValid = true;
        targetPosition = transform.position;
    }

    void Update()
    {
        /*Ray ray = new Ray(transform.position, transform.forward * 10);

        RaycastHit hit;

        Debug.DrawRay(transform.position, transform.forward * 10, Color.cyan);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.name == "Cube")
            {
                targetPosition = hit.point * 0.9f;

                Vector3 direction = transform.position - targetPosition;

                Scale(direction.magnitude);

                IsValid = true;
            }
        }
        else
        {
            IsValid = false;
            Scale(10);
        }*/
    }

    public void Scale(float scale)
    {
        scale = Mathf.Max(scale, 0);
        Visor.transform.localScale = new Vector3(Visor.transform.localScale.x, Visor.transform.localScale.y, scale);
    }

    /*private void OnCollisionStay(Collision collision)
    {
        if (collision.contacts .transform.name == "Cube")
        {
            other.
            Scale();
        }
    }*/
}
