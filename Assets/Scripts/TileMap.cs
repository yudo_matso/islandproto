﻿using UnityEngine;

public class TileMap : MonoBehaviour
{
    public GameObject[,] Tiles;
    public GameObject[,] Objects;
    public Vector2 Size { get; private set; }
    public Vector2 StartupTile { get; private set; }

    private Sprite[] _tileSprites;

    public void Init(int[,] tileMapDef, int[,] objectDef, Sprite[] tileSprites)
    {
        _tileSprites = tileSprites;

        float scale = 68 / _tileSprites[0].rect.width;
        transform.localScale = new Vector2(scale, scale);

        InitTiles(tileMapDef);
        InitObjects(objectDef);
    }

    private void InitTiles(int[,] tileMapDef)
    {
        Tiles = new GameObject[tileMapDef.GetLength(0), tileMapDef.GetLength(1)];

        for (int x = 0; x < Tiles.GetLength(0); x++)
        {
            for (int y = 0; y < Tiles.GetLength(1); y++)
            {
                GameObject go = new GameObject("Tile-" + x + "x" + y);
                go.transform.parent = transform;
                go.transform.localPosition = new Vector2(x * _tileSprites[0].rect.width + _tileSprites[0].rect.width * 0.5f, y * -_tileSprites[0].rect.height - _tileSprites[0].rect.height * 0.5f);
                go.transform.localScale = Vector3.one;
                Tiles[x, y] = go;

                if (tileMapDef[x, y] != 0)
                {
                    SpriteRenderer spriteRenderer = go.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = _tileSprites[tileMapDef[x, y]];
                    spriteRenderer.sortingLayerName = "TileMap";
                }
            }
        }

        Size = new Vector2(Tiles.GetLength(0) * _tileSprites[0].rect.width, Tiles.GetLength(1) * _tileSprites[0].rect.height);
    }

    private void InitObjects(int[,] objectDef)
    {
        Objects = new GameObject[Tiles.GetLength(0), Tiles.GetLength(1)];

        for (int x = 0; x < Tiles.GetLength(0); x++)
        {
            for (int y = 0; y < Tiles.GetLength(1); y++)
            {
                int spriteId = objectDef[x, y];

                switch (spriteId)
                {
                    case 0:
                        break;

                    case -1:
                        StartupTile = new Vector2(x, y);
                        break;

                    default:
                        GameObject go = new GameObject("Object");
                        go.transform.parent = Tiles[x, y].transform;
                        go.transform.localPosition = Vector3.zero;
                        go.transform.localScale = Vector3.one;

                        SpriteRenderer spriteRenderer = go.AddComponent<SpriteRenderer>();
                        spriteRenderer.sprite = _tileSprites[spriteId];
                        spriteRenderer.sortingLayerName = "Object";

                        go.AddComponent<BoxCollider2D>();
                        go.AddComponent<InteractiveObject>();

                        Objects[x, y] = go;
                        break;
                }
            }
        }
    }

}
