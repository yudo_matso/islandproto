﻿using UnityEngine;
using System.Collections;

public class InteractiveObject : MonoBehaviour {

    void Update()
    {
        if (Input.touchCount == 1)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            Process(wp);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Process(wp);
        }
    }

    private void Process(Vector3 wp)
    {
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        if (collider2D == Physics2D.OverlapPoint(touchPos))
        {
            Main.Instance.Fire(transform);
        }
    }
}
