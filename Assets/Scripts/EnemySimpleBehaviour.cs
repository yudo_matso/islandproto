﻿using System;
using UnityEngine;
using System.Collections;

public class EnemySimpleBehaviour : MonoBehaviour
{
    private Func<bool> _moveAction;
    private GameObject _player;

    public float constantSpeed  = 1.0f;
    public float smoothingFactor = 1.0f;

    void Awake()
    {
        rigidbody.interpolation = RigidbodyInterpolation.Interpolate;

        /*if (collider.material.name == "Default (Instance)")
        {
            PhysicMaterial pMat = new PhysicMaterial();
            pMat.name = "Frictionless";
            pMat.frictionCombine = PhysicMaterialCombine.Multiply;
            pMat.bounceCombine = PhysicMaterialCombine.Multiply;
            pMat.dynamicFriction = 0f;
            pMat.staticFriction = 0f;
            collider.material = pMat;
        }*/
    }

	void Start ()
	{
	    _player = GameObject.Find("Character");
	}


    private void FixedUpdate()
    {
        if (IsGrounded())
        {
            Quaternion newRotation = Quaternion.LookRotation(_player.transform.position - transform.position, Vector3.up);
            newRotation.x = 0;
            newRotation.z = 0;
            transform.rotation = newRotation;

            Vector3 relativePos = _player.transform.position - transform.position;
            relativePos.y = rigidbody.velocity.y;

            Vector3 velocityWihoutY = rigidbody.velocity;

            rigidbody.velocity = Vector3.Lerp(velocityWihoutY, relativePos.normalized * constantSpeed, Time.deltaTime*smoothingFactor);
        }
    }

    private bool IsGrounded()
    {
		float dist = collider.bounds.extents.y;

		RaycastHit hit;
        if (Physics.Raycast(collider.bounds.center, Vector3.down, out hit, dist + 0.05f, ~(1 << LayerMask.NameToLayer("Drag"))))
        {
            return true;
        }

        return false;
    }
}
