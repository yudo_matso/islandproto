﻿using System;
using Holoville.HOTween;
using UnityEngine;
using System.Collections;

public class CharacterWeapon : MonoBehaviour {

    private static int _fireState = Animator.StringToHash("Base.Fire"); 
    public GameObject Weapon;
    public Animator WeaponAnimator;
    public float FireDelay = 0.2f;
    public float ResetDelay = 0.2f;
    public float GrappleLength = 2.0f;

    public float FireSpeed = 1.0f;

    public event Action<Collision> Intercepted;

    private Renderer _renderer;
    private float _currentSpeed;
    private float _velocity = 0.0F;
    private Action _fireEnded;

    void Start()
    {
        Intercepted = delegate { };
    }

    public bool ScaleToPosition(Vector3 destination)
    {
        float scale = GrappleLength;

        float scaleTween = Mathf.SmoothDamp(Weapon.transform.localScale.z, scale, ref _velocity, 0.1f);
        Weapon.transform.localScale = new Vector3(Weapon.transform.localScale.x, Weapon.transform.localScale.y,
            scaleTween);

        if (Weapon.transform.localScale.z <= scale - 0.1f)
        {
            return false;
        }

        return true;
    }

    public bool ResetScale()
    {
        float scaleTween = Mathf.SmoothDamp(Weapon.transform.localScale.z, 0, ref _velocity, 0.1f);

        Weapon.transform.localScale = new Vector3(Weapon.transform.localScale.x, Weapon.transform.localScale.y,
            scaleTween);

        if (Weapon.transform.localScale.z > 0.1f)
        {
            return false;
        }

        return true;
    }

    void FixedUpdate()
    {
        AnimatorStateInfo currentState = WeaponAnimator.GetCurrentAnimatorStateInfo(0);

        if (WeaponAnimator.GetBool("Fire") && currentState.normalizedTime >= 0.75f || currentState.normalizedTime < 0)
        {
            WeaponAnimator.SetBool("Fire", false);
            WeaponAnimator.speed = 1;
            _fireEnded();
        }
    }
    public void Fire(Action callback)
    {
        _fireEnded = callback;
        WeaponAnimator.SetBool("Fire", true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (WeaponAnimator.GetBool("Fire"))
        {

            if (collision.gameObject.layer != LayerMask.NameToLayer("Enemy") && collision.gameObject.name != "Box01")
            {
                return;
            }
            Debug.Log("Collision !!!");
            WeaponAnimator.speed = -1;
            Intercepted(collision);
        }
    }

}
