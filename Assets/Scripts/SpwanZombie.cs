﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SpwanZombie : MonoBehaviour
{
    public Transform[] Spawns;
    public GameObject zombiePrefab;
    private List<GameObject> _zombieList;
    public GameObject player;

	void Start () 
    {
	    _zombieList = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    for (int i = 0; i < _zombieList.Count; i++)
	    {
	        GameObject go = _zombieList[i];
	        if (go.transform.position.y < -50)
	        {
	            _zombieList.Remove(go);
	            Destroy(go);
	        }
	    }

        if (player.transform.position.y < -50)
        {
            SpawnPlayer();
        }

        if (_zombieList.Count < 1)
        {
            SpawnZombie();
        }
	}

    private void SpawnPlayer()
    {
        player.transform.position = Spawns[0].position;
    }

    private void SpawnZombie()
    {
        int indexSpawn = Random.Range(0, Spawns.Length - 1);
        Transform spawn = Spawns[indexSpawn];

        _zombieList.Add((GameObject)Instantiate(zombiePrefab, spawn.position, Quaternion.identity));
    }
}
