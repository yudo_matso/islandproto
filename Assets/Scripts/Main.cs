﻿using System;
using System.IO;
using Holoville.HOTween;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Main : MonoBehaviour
{
    public static Main Instance { get; private set; }

    public TextAsset TileMapDefData;
    public TextAsset TileMapObjectDefData;
    public Sprite[] TileSprites;
    public TileMap TileMap;
    public SpriteRenderer Character;
    public Animator CharacterAnimator;
    public SpriteRenderer Weapon;

    private Texture2D _weaponTexture;
    private float _characterAngle;
    private int _direction = 0;
    private bool _isBlocked = false;

	void Start ()
	{
	    Instance = this;

        CharacterAnimator = Character.gameObject.GetComponent<Animator>();

        TileMap.Init(TileMapDefLoader.Instance.Parse(TileMapDefData), TileMapDefLoader.Instance.Parse(TileMapObjectDefData), TileSprites);

	    Vector2 tileMapSize = TileMap.Size;

        TileMap.transform.localPosition = new Vector2(Camera.main.rect.width * 0.5f - tileMapSize.x, Camera.main.rect.height * 0.5f + tileMapSize.y);

        TeleportCharacter(TileMap.Tiles[(int)TileMap.StartupTile.x, (int)TileMap.StartupTile.y].transform.position);

        _weaponTexture = new Texture2D(2, 2);

        for (int y = 0; y < _weaponTexture.height; y++)
        {
            for (int x = 0; x < _weaponTexture.width; x++)
            {
                _weaponTexture.SetPixel(x, y, Color.red);
            }
        }

        _weaponTexture.Apply(false);

        Sprite weaponSprite = Sprite.Create(_weaponTexture, new Rect(0, 0, _weaponTexture.width, _weaponTexture.height), Vector2.zero, 1);

        Weapon = new GameObject("Weapon").AddComponent<SpriteRenderer>();
	    Weapon.sprite = weaponSprite;
        Weapon.sortingLayerName = "Weapon";
	    Weapon.transform.localPosition = Character.transform.position;
	}

    private void Update()
    {
        Debug.Log("angle: " + _characterAngle);
        _direction = (Mathf.RoundToInt((360 - _characterAngle) / 45) - 2) % 8;
        CharacterAnimator.SetInteger("Direction", _direction);
        Debug.Log("direction: " + _direction);
    }

    public void Fire(Transform target)
    {
        if (_isBlocked)
        {
            return;
        }

        Weapon.transform.position = Character.transform.position;
        Weapon.transform.rotation = Quaternion.identity;
        Weapon.transform.localScale = Vector3.one;

        Vector3 dir = target.position - Weapon.transform.position;
        _characterAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Weapon.transform.rotation = Quaternion.AngleAxis(_characterAngle, Character.transform.forward);

        _isBlocked = true;
        Sequence sequence = new Sequence(new SequenceParms().OnComplete(() => { _isBlocked = false; }));
        
        sequence.Insert(0, HOTween.To(Weapon.transform, 0.2f, new TweenParms().Prop("localScale", new Vector3(dir.magnitude / 2, 1, 1))));
        sequence.Insert(0.2f, HOTween.To(Weapon.transform, 0.2f, new TweenParms().Prop("position", target.position).Ease(EaseType.EaseInExpo)));
        sequence.Insert(0.2f, HOTween.To(Weapon.transform, 0.2f, new TweenParms().Prop("localScale", new Vector3(0, 1, 1)).Ease(EaseType.EaseInExpo)));
        sequence.Insert(0.2f, HOTween.To(Character.transform, 0.2f, new TweenParms().Prop("position", new Vector3(target.position.x, target.position.y, -1)).Ease(EaseType.EaseInExpo)));

        sequence.Play();
    }

    public void TeleportCharacter(Vector2 targetPos)
    {
        Character.transform.localPosition = targetPos;
    }

}
