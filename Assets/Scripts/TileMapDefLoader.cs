﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class TileMapDefLoader : MonoBehaviour
{

    private static TileMapDefLoader _instance;
    public static TileMapDefLoader Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("TileMapDefLoader");
                _instance = go.AddComponent<TileMapDefLoader>();
            }

            return _instance;
        }
    }

    public int[,] Parse(TextAsset textAsset)
    {
        int[,] tileMapDef = null;

        using (StringReader reader = new StringReader(textAsset.text))
        {
            string line;
            List<string> lineList = new List<string>();

            while ((line = reader.ReadLine()) != null)
            {
                lineList.Add(line);
            }

            tileMapDef = new int[lineList[0].Split(',').Length, lineList.Count];

            for (int y = 0; y < tileMapDef.GetLength(1); y++)
            {
                string[] values = lineList[y].Split(',');

                for (int x = 0; x < tileMapDef.GetLength(0); x++)
                {
                    tileMapDef[x, y] = int.Parse(values[x]);
                }
            }
        }

        return tileMapDef;
    }
}
