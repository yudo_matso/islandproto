﻿using System;
using Holoville.HOTween;
using Holoville.HOTween.Core;
using UnityEngine;
using System.Collections;

public class CharacterMove : MonoBehaviour
{
    public float MoveSpeed = 6.0f;

    public bool MoveToPosition(Vector3 destination)
    {
        Vector3 relativePos = destination - transform.position;

        relativePos.y = destination.y = transform.position.y;

        transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * MoveSpeed);

        if (destination != transform.position)
        {
            return false;
        }

        return true;
    }


    public void LookAt(Vector3 destination)
    {
        Quaternion newRotation = Quaternion.LookRotation(destination, Vector3.up);
        newRotation.x = 0;
        newRotation.z = 0;

        transform.rotation = newRotation;
    }
}
