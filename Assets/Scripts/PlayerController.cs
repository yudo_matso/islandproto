﻿using System;
using System.Collections.Generic;
using Holoville.HOTween;
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public Animation AnimationComponent;
    public CharacterMove MoveComponent;
    public CharacterWeapon WeaponComponent;

    private bool _isOccupied = false;

    private Func<bool> _moveAction;
    private Func<bool> _fireAction;

    void Start()
    {
        WeaponComponent.Intercepted += Intercepted;
    }

    void FixedUpdate()
    {
        if (_moveAction != null && _moveAction())
        {
            _moveAction = null;
        }

        if (_fireAction != null && _fireAction())
        {
            _fireAction = null;
        }
    }

    private void Intercepted(Collision collision)
    {
        HOTween.To(transform, 0.2f, new TweenParms().Prop("position", collision.contacts[0].point));
    }

    public void Move(Vector3 targetPosition)
    {
        if (_isOccupied)
        {
            return;
        }
        rigidbody.velocity = Vector3.zero;

        Vector3 direction = transform.position - targetPosition;

        _moveAction = () =>
        {
            bool done = MoveComponent.MoveToPosition(targetPosition);
            MoveComponent.LookAt(-direction);
            AnimationComponent.CrossFade("run");

            if (done)
            {
                Debug.Log("finish");
                AnimationComponent.CrossFade("idle");
                return true;
            }

            return false;
        };
    }

    public void Fire(Vector3 targetPosition)
    {
        if(_isOccupied)
        {
            return;
        }

        _moveAction = null;
        _isOccupied = true;

        AnimationComponent.CrossFade("idle");

        Vector3 direction = transform.position - targetPosition;

        MoveComponent.LookAt(-direction);
        WeaponComponent.Fire(() => _isOccupied = false);
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer("Enemy") && collision.gameObject.name != "Cube")
        {
            return;
        }

        rigidbody.velocity = Vector3.zero;

        _moveAction = null;
        _isOccupied = false;
    }
}
